const twitterList = [
	{
		channel: 'AOD New York @aod',
		time: '35 min',
		content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut tellus ac nulla semper rhoncus. Nullam a odio porttitor, dictum turpis vitae, pretium ante amet.'
	},
	{
		channel: 'AOD Los Angeles @aod_la',
		time: '1 hour',
		content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut tellus ac nulla semper rhoncus.'
	},
	{
		channel: 'AOD San Francisco @aod_sa',
		time: '2 hours',
		content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut tellus ac nulla semper rhoncus. Nullam a odio porttitor, dictum turpis vitae, pretium ante amet...'
	}
];

function toggleMenu()
{
	const toggleValue = (element) =>
	{
		const el = document.getElementById(element);
		if(el.classList.contains('menu-opened'))
			el.classList.remove('menu-opened');
		else
			el.classList.add('menu-opened');
	}
	toggleValue('menu');
	toggleValue('burger');
	toggleValue('body');
	window.scrollTo({
		top: 0,
		behavior: "smooth"
	});
}

function sliderClick(e)
{
	const curElem = e.currentTarget;
	if(curElem.classList.contains('slider__item_active'))
		return;
	const sliderItems = document.querySelectorAll('.slider__item');
	sliderItems.forEach(item => {
		if(item.classList.contains('slider__item_active'))
			item.classList.remove('slider__item_active');
	});
	curElem.classList.add('slider__item_active');
	const twitterItems = document.querySelectorAll('.twitter__item');
	twitterItems.forEach(item => {
		if(item.classList.contains('twitter__item_active'))
			item.classList.remove('twitter__item_active');
	});
	twitterItems[curElem.dataset.index].classList.add('twitter__item_active');
}

function addTwitterItems(){
	addItem = (index) =>{
		const twitterContentDiv = document.createElement('div');
		twitterContentDiv.classList.add('twitter__item');
		twitterContentDiv.dataset.index = index;

		let newDiv = document.createElement('div');
		newDiv.classList.add('twitter__icon');

		let newImg = document.createElement('img');
		newImg.src = 'img/icon_twitter.png';
		newDiv.appendChild(newImg);

		twitterContentDiv.appendChild(newDiv);
		newDiv = document.createElement('div');
		newDiv.classList.add('twitter__source');
		newDiv.textContent = twitterList[index].channel;
		newDiv.appendChild(document.createElement('span'));
		newDiv.querySelector('span').textContent = ' / ' + twitterList[index].time;
		twitterContentDiv.appendChild(newDiv);

		newDiv = document.createElement('div');
		newDiv.classList.add('twitter__text');
		newDiv.textContent = twitterList[index].content;
		twitterContentDiv.appendChild(newDiv);

		document.querySelectorAll('.twitter__item')[document.querySelectorAll('.twitter__item').length - 1].insertAdjacentElement('afterend', twitterContentDiv);
	}
	addItem(1);
	addItem(2);
}

const slider = document.querySelectorAll('.slider__item');
slider.forEach(item => {
	item.addEventListener('click', sliderClick);
});

addTwitterItems();